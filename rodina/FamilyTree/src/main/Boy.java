package main;

public class Boy extends Person implements Visitable{

    private Boy(Person mother, Person father) {
        super(mother, father);
    }

    private static Boy boy = new Boy(mother, father);

    public static Boy getBoy(){
        return boy;
    }

    @Override
    public  Person reproduce(Person person){
        if (person instanceof Girl){
            person = new Person(person, boy);
        }
        return person;
    }

    @Override
    public void accept(FamilyVisitor familyVisitor) {
        familyVisitor.visit(Boy.getBoy());
    }
}
