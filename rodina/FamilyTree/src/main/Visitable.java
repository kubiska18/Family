package main;

public interface Visitable {
    void accept(FamilyVisitor familyVisitor);
}
