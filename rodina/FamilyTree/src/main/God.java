package main;

import static main.Boy.getBoy;
import static main.Girl.getGirl;

public class God{

    public static God instance = null;

    private God(){
    }

    public static God getInstance(){
        if (instance == null){
            instance = new God();
        }
        return instance;
    }

    public final Girl createGirl(){
//        System.out.println("Girl was created.");
        return getGirl();
    }

    public final Boy createBoy(){
//        System.out.println("Boy was created.");
        return getBoy();

    }

    @Override
    public String toString() {
        return "This is God!";
    }
}
