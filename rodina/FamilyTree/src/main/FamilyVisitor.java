package main;

public class FamilyVisitor {

    public static void visit(Girl girl){
        System.out.println("Hey baby girl " + girl.getName());
    }

    public static void visit(Boy boy){
        System.out.println("Hey baby boy " + boy.getName() );
    }

    public void visit(Family family){
        System.out.println("Hey my new family " + family.father.getSurname());
    }
}
