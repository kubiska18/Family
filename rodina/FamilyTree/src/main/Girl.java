package main;

public class Girl extends Person implements Visitable{

    private Girl(Person mother, Person father) {
        super(mother, father);
    }

    private static Girl girl = new Girl(mother, father);

    public static Girl getGirl() {
        return girl;
    }

    @Override
    public Person reproduce(Person person){
        if (person instanceof Boy){
            person = new Person(girl, person);
        }
        return person;
    }

    @Override
    public void accept(FamilyVisitor familyVisitor) {
        familyVisitor.visit(Girl.getGirl());
    }
}
