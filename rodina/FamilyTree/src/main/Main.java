package main;


import gui.Gui;

import static main.Person.*;

public class Main {

    public static void main(String[] args) {
        Gui gui = new Gui();
        gui.load();

        FamilyVisitor familyVisitor = new FamilyVisitor();
        God god = God.getInstance();

        //God creates first boy and girl
        Boy myBoy = god.createBoy();
        Girl myGirl = god.createGirl();
        myBoy.setName("Adam");
        myBoy.setSurname("First");
        myGirl.setName("Eva");
        myGirl.setSurname("Second");
        System.out.println("These are my first two people created by God");
        System.out.println(myGirl.toString());
        System.out.println(myBoy.toString());
        System.out.println("\n");


        //Get married
        marriage(myGirl, myBoy);

        //Add my girl and my boy to my new family
        Family myFamily = new Family(myGirl, myBoy);
        System.out.println("They got married so they have the same surname");
        System.out.println(myFamily.getOne().toString());
        System.out.println(myFamily.getOther().toString());
        System.out.println("\n");

        //Reproduce
        Person firstChild;
        firstChild = myGirl.reproduce(myBoy);
        firstChild.setFamily(myFamily);
        firstChild.setName("Alex");
        firstChild.setSurname(myFamily.father.getSurname());
        myFamily.addChild(firstChild);
        Person secondChild = myBoy.reproduce(myGirl);
        secondChild.setFamily(myFamily);
        secondChild.setName("Filipko");
        secondChild.setSurname(myFamily.father.getSurname());
        myFamily.addChild(secondChild);
        System.out.println("Let's introduce my new family");
        System.out.println(firstChild.getFamily());
        System.out.println("\n");
        System.out.println("And add our new child to our family");
        System.out.println(getChildren(myFamily));
        System.out.println("\n");
        System.out.println("This is the list of our family");
        System.out.println(myFamily.getMembers().toString());
        System.out.println("\n");


        //Greet my family and members
        System.out.println("And finally greet them all");
        familyVisitor.visit(myBoy);
        familyVisitor.visit(myGirl);
        familyVisitor.visit(myFamily);

//        System.out.println("Father of " + secondChild.getName() + " is " + secondChild.getFather());
    }
}
