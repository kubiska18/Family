package main;

import java.util.HashSet;
import java.util.Set;

public class Family implements Visitable{
    Person mother, father;
    static Set<Person> familySet = new HashSet<Person>();
    static Set<Person> children = new HashSet<Person>();

    public Family(Person mother, Person father){
        this.mother = mother;
        this.father = father;
        familySet.add(mother);
        familySet.add(father);
    }

    public Person getOne() {
        return mother;
    }

    public Person getOther() {
        return father;
    }

    public static void addChild(Person person){
        children.add(person);
        familySet.add(person);
    }

    public static Set<Person> getMembers() {
        return familySet;
    }

    @Override
    public void accept(FamilyVisitor familyVisitor) {
        familyVisitor.visit((Family) Family.getMembers());
    }

    @Override
    public String toString() {
        return "This is family " + father.getSurname() + ".";
    }
}
