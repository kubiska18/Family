package main;

import java.util.Set;

public class Person implements Visitable {
    public  String name, surname;
    static Person father, mother;
    static Family family;

    public Person(Person mother, Person father){
        this.mother = mother;
        this.father = father;
    }

    public static Person getFather() {
        return father;
    }

    public static Person getMother() {
        return mother;
    }

    public String getName() {
        return this.name;
    }

    public String getSurname() {
        return this.surname;
    }

    public static Family getFamily() {
        return family;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public static Set<Person> getChildren(Family family){
        return family.children;
    }

    public static Family marriage(Person woman, Person man){
        woman.setSurname(man.getSurname());
        return new Family(woman, man);
    }

    public Person reproduce(Person person){
        Person child;
        if (person != null){
            child = new Person(getMother(), getFather());
            return child;
        }
        else {
            return null;
        }
    }

    @Override
    public void accept(FamilyVisitor familyVisitor) {
        familyVisitor.visit(family);
    }

    @Override
    public String toString() {
        return this.getName() + " " + this.getSurname();
    }
}
