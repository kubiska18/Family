package gui;

import javax.swing.*;
import java.awt.*;

public class Gui {

    public static void load(){
        createFrame();
    }

    private static void createFrame() {
        JFrame frame = new JFrame("Family");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(800, 500);
        frame.setVisible(true);
        frame.setLocation(400,100);
        ImageIcon icon = new ImageIcon("images/tree.png");
        frame.setIconImage(icon.getImage());

        JPanel panelGod = new JPanel();
        ImageIcon imageIconGod = new ImageIcon("images/jesuschrist.jpg");
        Image imageGod = imageIconGod.getImage();
        Image scaledImageGod = imageGod.getScaledInstance(300,180,Image.SCALE_SMOOTH);
        imageIconGod = new ImageIcon(scaledImageGod);
        JLabel labelGod = new JLabel("First there were God who created one boy and one girl", imageIconGod, JLabel.CENTER);
        panelGod.add(labelGod);
        frame.add(panelGod, BorderLayout.NORTH);
        frame.setVisible(true);

        JPanel panelBoy = new JPanel();
        ImageIcon imageIconBoy = new ImageIcon("images/man.jpg");
        Image imageBoy = imageIconBoy.getImage();
        Image scaledImageBoy = imageBoy.getScaledInstance(150,100,Image.SCALE_SMOOTH);
        imageIconBoy = new ImageIcon(scaledImageBoy);
        JLabel labelBoy = new JLabel("This is Boy", imageIconBoy, JLabel.CENTER);
        panelBoy.add(labelBoy);
        frame.add(panelBoy, BorderLayout.WEST);
        frame.setVisible(true);

        JPanel panelGirl = new JPanel();
        ImageIcon imageIconGirl = new ImageIcon("images/woman.jpg");
        Image imageGirl = imageIconGirl.getImage();
        Image scaledImageGirl = imageGirl.getScaledInstance(150,100,Image.SCALE_SMOOTH);
        imageIconGirl = new ImageIcon(scaledImageGirl);
        JLabel labelGirl = new JLabel("This is Girl     ", imageIconGirl, JLabel.CENTER);
        panelGirl.add(labelGirl);
        frame.add(panelGirl, BorderLayout.EAST);
        frame.setVisible(true);

        JPanel panelChild = new JPanel();
        ImageIcon imageIconChild = new ImageIcon("images/child.jfif");
        Image imageChild = imageIconChild.getImage();
        Image scaledImageChild = imageChild.getScaledInstance(150,100,Image.SCALE_SMOOTH);
        imageIconChild = new ImageIcon(scaledImageChild);
        JLabel labelChild = new JLabel("My boy and girl got married and they gave life to this little child", imageIconChild, JLabel.CENTER);
        panelChild.add(labelChild);
        frame.add(panelChild, BorderLayout.SOUTH);
        frame.setVisible(true);


//        JLabel labelGod = new JLabel("God");
//        labelGod.setSize(100,100);
//        labelGod.setFont(new Font("Serif", Font.PLAIN, 26));
//        labelGod.setLocation(450, 10);
//        labelGod.setVisible(true);
//        frame.add(labelGod);
//        JLabel labelBoy = new JLabel("Boy");
//        labelBoy.setSize(100,100);
//        labelBoy.setFont(new Font("Serif", Font.PLAIN, 26));
//        labelBoy.setLocation(250,100);
//        labelBoy.setVisible(true);
//        frame.add(labelBoy);
//        JLabel labelGirl = new JLabel("Girl");
//        labelGirl.setSize(100,100);
//        labelGirl.setFont(new Font("Serif", Font.PLAIN, 26));
//        labelGirl.setLocation(650,100);
//        labelGirl.setVisible(true);
//        frame.add(labelGirl);
//        JLabel labelChild = new JLabel("Child");
//        labelChild.setSize(100,100);
//        labelChild.setFont(new Font("Serif", Font.PLAIN, 26));
//        labelChild.setLocation(450,200);
//        labelChild.setVisible(true);
//        frame.add(labelChild);
    }



}
